package common;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

import java.util.HashMap;
import java.util.Map;

public class RequestHelper {

        public static void setParams(Map<String, Object> params) {
            RequestHelper.params = params;
        }

        private static Map<String, Object> params = new HashMap<>();


        public static Map<String, Object> getParams() {
            return params;
        }

        public static void setReqParameters(String paramKey, Object paramValue){
            params.put(paramKey, paramValue);
        }

        public static RequestSpecification getRequestSpecification(String URI){
            RestAssured.baseURI = URI;
            return  RestAssured
                    .given()
                    .contentType(ContentType.JSON);

        }
    }

