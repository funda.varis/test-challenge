package common;

import apiResources.Endpoint;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ResponseHelper {

    protected static Response response;
    private static Logger log = LogManager.getLogger(ResponseHelper.class);

        public static Response getResponse(RequestSpecification request , Endpoint endpoint){
            String path = endpoint.getPath();
            String api = endpoint.toString();

            if(api.startsWith("Post")){
                response = request.when().post(path);
            }
            else if(api.startsWith("Get")) {
                response=request.when().get(path);
            }
            else if(api.startsWith("Delete")) {
                response=request.when().delete(path);
            }
            else if (api.startsWith("Put")) {
                response=request.when().put(path);
            }
            return response;
        }

        public static Response getResponse(RequestSpecification request , Endpoint endpoint, String newEndpoint){
            String api = endpoint.toString();

            if(api.startsWith("Post")){
                response = request.when().post(newEndpoint);
            }
            else if(api.startsWith("Get")) {
                response=request.when().get(newEndpoint);
            }
            else if(api.startsWith("Delete")) {
                response=request.when().delete(newEndpoint);
            }
            else if (api.startsWith("Put")) {
                response=request.when().put(newEndpoint);
            }
            return response;
        }

        public static void checkStatusCode(Response response, int status){
            if(status ==  response.getStatusCode() ){
                log.info(String.valueOf(response.getStatusCode()),response.getBody());
            }else {
               log.error(String.valueOf(response.getStatusCode()), status, response.getBody());
            }
        }
    }

