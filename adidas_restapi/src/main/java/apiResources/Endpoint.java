package apiResources;

public enum Endpoint {

        GetAllProducts("/Product"),
        PostCreateProduct("/Product"),
        GetSingleProduct("/Product/{id}"),
        PutSingleProduct("/Product/{id}"),
        DeleteProduct("/Product/{id}"),
        GetPriceForProduct("/Product/{id}");


        private String path;

        Endpoint(String path)
        {
            this.path = path;
        }

        public  String getPath()
        {
            return path;
        }
    }

