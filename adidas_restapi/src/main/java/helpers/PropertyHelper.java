package helpers;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertyHelper {

        private static final String propertyFolder = "src/main/resources/";
        private static Properties prop = new Properties();

        public static String getPropValue(String property) {
            String value = "";
            try
            {
                FileInputStream fileInputStream = new FileInputStream(propertyFolder + "config.properties");
                prop.load(fileInputStream);
                value = prop.getProperty(property);
                fileInputStream.close();
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            return value;
        }
    }
