package tests;

import apiResources.Endpoint;
import common.ResponseHelper;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static common.RequestHelper.*;
import static helpers.PropertyHelper.getPropValue;

public class ProductTests {
    Response response;
    RequestSpecification request;
    private static String baseURI =getPropValue("baseURI");
    private static String priceBaseURI =getPropValue("priceBaseURI");
    private static Logger log = LogManager.getLogger(ProductTests.class);

    @Test
    @DisplayName("Get All Products with price")
    public void getAllProducts(){
        RequestSpecification request  = getRequestSpecification(baseURI);
        response = ResponseHelper.getResponse(request, Endpoint.GetAllProducts);
        ResponseHelper.checkStatusCode(response,200);
        log.info(response);
    }

    @Test
    @DisplayName("Creates a product in the database")
    public void createProductsInDb(){
        setReqParameters("id","id");
        setReqParameters("name","name");
        setReqParameters("description","asd");
        request = getRequestSpecification(baseURI).queryParams(getParams());
        response = ResponseHelper.getResponse(request, Endpoint.PostCreateProduct);
        log.info(response);
        ResponseHelper.checkStatusCode(response,200);

    }

    @Test
    @DisplayName("Get a single product")
    public void getSingleProduct(){
        request = getRequestSpecification(baseURI);
        String endpoint = Endpoint.GetSingleProduct.getPath().replace("{id}", "1234");
        response = ResponseHelper.getResponse(request, Endpoint.GetSingleProduct, endpoint);
        log.info(request);
        ResponseHelper.checkStatusCode(response,200);
    }

    @Test
    @DisplayName("Update a product")
    public void updateProduct(String id){
        request = getRequestSpecification(baseURI);
        String endpoint = Endpoint.PutSingleProduct.getPath().replace("{id}", id);
        response = ResponseHelper.getResponse(request, Endpoint.PutSingleProduct,endpoint);
        ResponseHelper.checkStatusCode(response,200);
    }

    @Test
    @DisplayName("Delete a product")
    public void deleteProduct(String id){
        request = getRequestSpecification(baseURI);
        String endpoint = Endpoint.DeleteProduct.getPath().replace("{id}", id);
        response = ResponseHelper.getResponse(request, Endpoint.DeleteProduct, endpoint);
        ResponseHelper.checkStatusCode(response,200);
    }

    @Test
    @DisplayName("Get price of a product")
    public void getPriceForAProduct(String id){
        request = getRequestSpecification(priceBaseURI);
        String endpoint = Endpoint.GetPriceForProduct.getPath().replace("{id}", id);
        response = ResponseHelper.getResponse(request, Endpoint.GetPriceForProduct,endpoint);
        ResponseHelper.checkStatusCode(response,200);
    }



}
