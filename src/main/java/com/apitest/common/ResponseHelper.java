package com.apitest.common;

import com.apitest.api_resources.HttpOperations;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;


public class ResponseHelper {
    ValidatableResponse resp;
    private static final Logger log = LogManager.getLogger(ResponseHelper.class);

    public ValidatableResponse getResponse(RequestSpecification request, HttpOperations operations) {
        switch (operations.toString()) {
            case "POST":
                resp = RestAssured.given(request).log().all().when().post().then().log().all();
                break;
            case "GET":
                resp = RestAssured.given(request).log().all().when().get().then().log().all();
                break;
            case "PUT":
                resp = RestAssured.given(request).log().all().when().put().then().log().all();
                break;
            case "DELETE":
                resp = RestAssured.given(request).log().all().when().delete().then().log().all();
                break;
            case "PATCH":
                resp = RestAssured.given(request).log().all().when().patch().then().log().all();
                break;
            default:
                log.info("There is no request sent");
        }
        return resp;
    }

    public String getResponseString() {
        return resp.extract().asPrettyString();
    }


    public void checkStatusCode(int status) {
        Response rsp=resp.extract().response();
        if (status == rsp.getStatusCode())
            log.info("Response body {} ", getResponseString());
        else
            log.error("Response body {} ", getResponseString());
    }

    public void assertStatusCode(int status) {
        Response rsp=resp.extract().response();
        Assert.assertEquals(rsp.getStatusCode(), status, "Status Check Failed!");}

    public static JsonPath getJsonPath(Response res) {
        String json = res.asString();
        return new JsonPath(json);
    }

}

