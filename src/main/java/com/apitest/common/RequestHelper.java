package com.apitest.common;

import com.apitest.api_resources.Endpoint;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.apitest.helpers.PropertyHelper.getPropValue;

public class RequestHelper {
    RequestSpecification reqSpec;
    RequestSpecBuilder reqBuilder = new RequestSpecBuilder();
    private final static Logger log = LogManager.getLogger(RequestHelper.class);

    public RequestSpecification setBaseURI(String baseConstant) {
        reqBuilder.setBaseUri(getPropValue(baseConstant));
        reqSpec = reqBuilder.build();
        return reqSpec;
    }

    public RequestSpecification setBasePath(Endpoint endpoint) {
        String path = endpoint.getPath();
        reqBuilder.setBasePath(path);
        reqSpec = reqBuilder.build();
        return reqSpec;
    }

    public RequestSpecification setBody(String json) {
        reqBuilder.setBody(json);
        reqSpec = reqBuilder.build();
        return reqSpec;
    }

    public static void resetBaseURI() {
        RestAssured.baseURI = null;
    }

    public void setPathParam(String key, Object val) {
        reqBuilder.addPathParam(key, val);
        reqSpec = reqBuilder.build();
        log.info(reqSpec.log());
    }

    public void setAcceptLang(String lang) {
        reqBuilder.addHeader("Accept-Language", lang);
        reqSpec = reqBuilder.build();
    }
}

