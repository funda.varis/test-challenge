package com.apitest.api_resources;

public enum Endpoint {

    PRODUCT_PATH("/Product"),
    PRODUCT_ID("/Product/{id}");


    private String path;

    Endpoint(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}

