package com.apitest.api_resources;

public enum HttpOperations {
    GET, PUT, POST, DELETE, PATCH;

}
