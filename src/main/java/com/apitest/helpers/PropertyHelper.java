package com.apitest.helpers;


import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class PropertyHelper {

    private static final String RSC_FOLDER = "src/main/resources/";
    private static final Properties prop = new Properties();


    public static String getPropValue(String property) {
        String value = "";
        try (FileInputStream fileInputStream = new FileInputStream(RSC_FOLDER + "config.properties")) {
            prop.load(fileInputStream);
            value = prop.getProperty(property);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return value;
    }

    public static String readJsonData(String path) {
        JSONObject jsonObject = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader(RSC_FOLDER + "product.json"));

            jsonObject = (JSONObject) obj;

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        JSONObject x = (JSONObject) jsonObject.get(path);

        return x.toString();
    }
}
