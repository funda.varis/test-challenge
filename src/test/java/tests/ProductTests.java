package tests;

import com.apitest.api_resources.Endpoint;
import com.apitest.api_resources.HttpOperations;
import com.apitest.common.RequestHelper;
import com.apitest.common.ResponseHelper;
import io.restassured.path.json.JsonPath;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import static com.apitest.helpers.PropertyHelper.readJsonData;

public class ProductTests {
    RequestHelper reqHelper = new RequestHelper();
    ResponseHelper respHelper = new ResponseHelper();
    ValidatableResponse response;
    RequestSpecification request;
    private static final Logger log = LogManager.getLogger(ProductTests.class);

    @Test(description = "Get All Products with price")
    public void getAllProducts() {
        reqHelper.setBaseURI("baseURI");
        request = reqHelper.setBasePath(Endpoint.PRODUCT_PATH);
        response = respHelper.getResponse(request, HttpOperations.GET);
        respHelper.assertStatusCode(200);
    }

    @Test(description = "Creates a product in the database")
    public void createProductsInDb() {
        String json = readJsonData("product_1");
        reqHelper.setBaseURI("baseURI");
        request = reqHelper.setBasePath(Endpoint.PRODUCT_PATH);
        reqHelper.setBody(json);
        log.info(json);
        response = respHelper.getResponse(request, HttpOperations.POST);
        respHelper.checkStatusCode(200);
    }


    @Test(description = "Get a single product detail")
    public void getSingleProduct() {
        reqHelper.setBaseURI("baseURI");
        request = reqHelper.setBasePath(Endpoint.PRODUCT_ID);
        reqHelper.setPathParam("id", "asdf");
        response = respHelper.getResponse(request, HttpOperations.GET);
        respHelper.checkStatusCode(200);
    }

    @Test(description = "Update a single product in database")
    public void updateProduct() {
        reqHelper.setBaseURI("baseURI");
        reqHelper.setBasePath(Endpoint.PRODUCT_ID);
        reqHelper.setPathParam("id", "asdf");
        String json = readJsonData("product_1");
        request = reqHelper.setBody(json);
        response = respHelper.getResponse(request, HttpOperations.PUT);
    }

    @Test(description = "Delete a product in the database")
    public void deleteProduct() {
        reqHelper.setBaseURI("baseURI");
        request = reqHelper.setBasePath(Endpoint.PRODUCT_ID);
        reqHelper.setPathParam("id", "asdf");
        response = respHelper.getResponse(request, HttpOperations.DELETE);
    }

    @Test(description = "Get price of a product")
    public void getPriceForProduct() {
        reqHelper.setBaseURI("priceBaseURI");
        request = reqHelper.setBasePath(Endpoint.PRODUCT_ID);
        reqHelper.setPathParam("id", "asdf");
        reqHelper.setAcceptLang("es-ES");
        response = respHelper.getResponse(request, HttpOperations.GET);
        JsonPath jsonPath = respHelper.getJsonPath(response.extract().response());
        log.info(jsonPath.get("price").toString());
    }

}
